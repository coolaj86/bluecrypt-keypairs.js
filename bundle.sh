#!/bin/bash

# Development Version
cat > bluecrypt-keypairs.js << EOF
// Copyright 2015-2019 AJ ONeal. All rights reserved
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
;
EOF
cat ./lib/encoding.js \
  ./lib/asn1-packer.js \
  ./lib/x509.js \
  ./lib/ecdsa.js \
  ./lib/rsa.js \
  ./lib/keypairs.js \
  >> bluecrypt-keypairs.js

# Gzipped
cat > bluecrypt-keypairs.min.js << EOF
// Copyright 2015-2019 AJ ONeal. All rights reserved
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
;
EOF
uglifyjs bluecrypt-keypairs.js >> bluecrypt-keypairs.min.js
gzip -f bluecrypt-keypairs.min.js

# Minified Gzipped
cat > bluecrypt-keypairs.min.js << EOF
// Copyright 2015-2019 AJ ONeal. All rights reserved
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
;
EOF
uglifyjs bluecrypt-keypairs.js >> bluecrypt-keypairs.min.js

rsync -av ./ root@beta.therootcompany.com:~/beta.therootcompany.com/keypairs/
rsync -av ./ root@beta.rootprojects.org:~/beta.rootprojects.org/keypairs/
rsync -av ./ ubuntu@rootprojects.org:/srv/www/rootprojects.org/keypairs/
