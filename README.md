# Bluecrypt&trade; [Keypairs](https://git.rootprojects.org/root/bluecrypt-keypairs.js) | A [Root](https://rootprojects.org) Project

A port of [keypairs.js](https://git.coolaj86.com/coolaj86/keypairs.js) to the browser.

# Features (port in-progress)

  * [x] Keypair generation and encoding
    * [x] RSA
    * [x] ECDSA (P-256, P-384)
    * [x] JWK-to-PEM
    * [ ] JWK-to-SSH
    * [ ] PEM-to-JWK
    * [ ] SSH-to-JWK
    * [x] ASN1, X509, PEM, DER
  * [x] SHA256 JWK Thumbprints
  * [x] Sign JWS
  * [ ] Create JWTs
  * [ ] JWK fetching. See [Keyfetch.js](https://npmjs.com/packages/keyfetch/)
    * [ ] OIDC
    * [ ] Auth0
  * [ ] CLI (ee [keypairs-cli](https://npmjs.com/packages/keypairs-cli/))
  * [ ] Node.js (ee [keypairs.js](https://npmjs.com/packages/keypairs.js))
  * [ ] [CSR.js](https://git.rootprojects.org/root/bluecrypt-csr.js)
  * [ ] [ACME.js](https://git.rootprojects.org/root/bluecrypt-acme.js) (Let's Encyrpt)

# Online Demos

* Bluecrypt Keypairs.js Demo <https://rootprojects.org/keypairs/>

# QuickStart

`bluecrypt-keypairs.js`
```html
<script src="https://rootprojects.org/keypairs/bluecrypt-keypairs.js"></script>
```

`bluecrypt-keypairs.min.js`
```html
<script src="https://rootprojects.org/keypairs/bluecrypt-keypairs.min.js"></script>
```

You can see `index.html` and `app.js` in the repo for full example usage.

# Documentation

See [keypairs.js](https://git.coolaj86.com/coolaj86/keypairs.js) for documentation.
